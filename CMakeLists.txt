cmake_minimum_required(VERSION 3.5)
project("UniCAN++" VERSION 0.1)

# Set C++ standard
set(CMAKE_CXX_STANDARD 11)

include_directories(include)

# Enable debug symbols by default
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Debug
            CACHE STRING
            "Choose the type of build (Debug or Release)" FORCE)
endif()


# Export compile commands for completion engines (optional)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)


add_subdirectory(src)


#add_subdirectory(test)
