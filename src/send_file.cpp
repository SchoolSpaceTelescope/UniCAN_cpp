#include <cstring>
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>

#include "drivers/UniCAN_Linux.hpp"
#include "drivers/UniCAN_Test.hpp"

// TODO: unify CAN interface
int SendFile(std::ifstream &file, UniCAN::LinuxCAN& unican){
    if (!file.is_open()) return -1;

    file.seekg(0, std::ios_base::end);
    std::streampos size = file.tellg();

    std::cout << "file length: " << size << std::endl;

    file.seekg(0, std::ios_base::beg);

    UniCAN::Message start_msg(980, 1, 31, sizeof(size));


    std::memcpy(start_msg.getData(), &size, sizeof(size));

    unican.send(start_msg);



    const std::streampos pack_max_size = 1000;

    UniCAN::Message pack_msg(981, 1, 31, 0);

    auto *data = new uint8_t[pack_max_size];

//    msg->unican_msg_id = 981;

    std::streampos current_size = 0;
    std::streampos pack_size;

//    msg->data = static_cast<uint8 *>(malloc(sizeof(uint8_t) * pack_max_size));

    while (current_size < size){
//        sleep(1);
        if (size - current_size < pack_max_size){
            pack_size = size - current_size;
        } else pack_size = pack_max_size;

        pack_msg.setData(data, pack_size);

//        pack_msg.getLength() = pack_size;

        file.read(reinterpret_cast<char *>(pack_msg.getData()), pack_size);

        unican.send(pack_msg);

        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        current_size += pack_size;
    }

//    FILE *pFile;
//    pFile = fopen ("newfile.txt", "w");
//    fwrite (msg->data , 1 , size, pFile );


//    free(msg->data);




    UniCAN::Message end_msg(982, 1, 31, 0);

//    msg->unican_msg_id = 982;
//    msg->unican_length = 0;

    unican.send(end_msg);

//    unican_send_message(msg);

//    free(msg->data);

//    free(msg);

//    std::cout << "SENT file length: " << size << std::endl;

    return 0;
}

int main(){
    UniCAN::LinuxCAN unican;

    unican.open("vcan0");

    std::ifstream fin("input.txt");

    SendFile(fin, unican);

    fin.close();

    return 0;
}
