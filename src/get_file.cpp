#include <cstring>
#include <iostream>
#include <fstream>
#include <cstdio>

#include "drivers/UniCAN_Linux.hpp"
#include "drivers/UniCAN_Test.hpp"

std::streampos size = 0;
std::streampos received_size = 0;
bool can_exit = false;

//auto prev_file_time = std::chrono::system_clock::now();

void FileControl(UniCAN::Message& msg){
    if (msg.getId() == 982) {
        std::cout << "FILE SIZE " << size << " success " << received_size <<
                  " MISSED " << size - received_size << std::endl;
        size = 0;
//        received_size = 0;

        can_exit = true;
    }

    if (msg.getId() == 980) {
        std::memcpy(&size, msg.getData(), sizeof(size));

        std::cout << "FILE SIZE " << size << " RECEIVE STARTED\n";

        received_size = 0;
    }

//    std::cout.flush();
//     exit(0);

}

void GetFile(UniCAN::Message& msg){
//    if (msg->unican_msg_id == 980){
    std::streampos pack_size = msg.getLength() - 2;
//            std::memcpy(&size, msg->data, sizeof(size));

//            GetFile(size);
//        }


// Some computation here
//    auto end = std::chrono::system_clock::now();
//
//    std::chrono::duration<double> elapsed_seconds = end-prev_file_time;
//    std::time_t end_time = std::chrono::system_clock::to_time_t(end);


//    prev_file_time = end;



    std::ofstream newfile;

    if (received_size == 0) {
        newfile.open("received.bin");
    }else{
        newfile.open("received.bin", std::fstream::app);
    }

    received_size += pack_size;

    newfile.write(reinterpret_cast<const char *>(msg.getData()), pack_size);

    newfile.close();

    std::cout << "GET PACK SIZE: " <<
              //        "elapsed time: " << elapsed_seconds.count() << " s " <<
              pack_size << " RECEIVED " << received_size << "/" << size << std::endl;
}

/*
This function should perform required software
reaction to unican message received from another
device.
*/
UniCAN::Message unican_RX_message (UniCAN::Message msg)
{
    /* User code */



//    printf("RECEIVED: %u %u %u %u\n", msg.getId(), msg.getAddressFrom(), msg.getAddressTo(), msg.getLength());

//    for (int i = 0; i < msg.getLength(); ++i){
//        printf("[%d] %02x\n", i, msg.getData()[i]);
//    }

    FileControl(msg);

//    printf("\n");


    if (msg.getId() == 981) GetFile(msg);

    return msg;

    /*end of user code*/
}

int main(){
    UniCAN::LinuxCAN unican{};

    unican.open("vcan0");

    while (!can_exit){
//        sleep(1);
//        std::cout << "Take message\n";
//        can_HW_receive_message();
        UniCAN::ErrorCode errorCode = unican.collect_frame();

        if (errorCode != UniCAN::ErrorCode::UNICAN_OK){
            std::cerr << "UNICAN ERROR: " << (int) unican.collect_frame() << std::endl;
        }

//        printf("Frame info: %d\n", );

        if (!unican.processed_messages.empty()){
            unican_RX_message(unican.take_message());
        }

//        unican_take_message();
//        printf("RECEIVED2: %u %u %u %u\n", msg->unican_msg_id, msg->unican_address_from, msg->unican_address_to, msg->unican_length);

//
    }

    unican.close();

    return 0;
}
