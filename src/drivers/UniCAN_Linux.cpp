/* SPDX-License-Identifier: (GPL-2.0-only OR BSD-3-Clause) */
/*
 * cansend.c - send CAN-frames via CAN_RAW sockets
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 *
 */


#include "drivers/UniCAN_Linux.hpp"

using namespace UniCAN;


void LinuxCAN::open(std::string can_name) {
    sockaddr_can addr{};

    ifreq ifr{};

    /* open socket */
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("socket");
        return ;
    }

    strncpy(ifr.ifr_name, can_name.c_str(), IFNAMSIZ - 1);
    ifr.ifr_name[IFNAMSIZ - 1] = '\0';
    ifr.ifr_ifindex = if_nametoindex(ifr.ifr_name);
//    ifr.ifr_ifindex = 0;  // ALL_CAN


    if (!ifr.ifr_ifindex) {
        perror("if_nametoindex");
        return ;
    }

    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("bind");
        return ;
    }

    Base::online = true;
}

void LinuxCAN::HWsend(Frame &frame) {
    // TODO: rename frame and canframe

    printf("try send1: %d \n", frame.can_dlc);
    /* HW depended user code */
    int required_mtu = CAN_MTU;

    can_frame canframe{}; // TODO: add CAN FD support, use canfd_frame

    /* parse CAN frame */
//     required_mtu = parse_canframe("001#DEADBEEF", &frame);
//
//     /* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
//     frame.len = can_fd_dlc2len(can_fd_len2dlc(frame.len));

    canframe.can_id = frame.can_identifier;
    canframe.can_id |= frame.can_extbit;
    canframe.can_id |= frame.can_rtr;
    canframe.len = frame.can_dlc;

    memcpy(canframe.data, (const void *) frame.data, sizeof(frame.data));

    // TODO: add CAN FD support
    /* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
//    canframe.len = can_fd_dlc2len(can_fd_len2dlc(canframe.len));

    /* send frame */
    if (write(s, &canframe, required_mtu) != required_mtu) {
        perror("write");
//        return -1;
    }

    printf("try send2\n");
}

Frame LinuxCAN::HWreceive() {
    Frame frame{};

//    can_message frame;
    /* HW depended user code */

//    printf("can start\n");
    struct can_frame canFrame;

    ssize_t nbytes = read(s, &canFrame, sizeof(struct can_frame));

    if (nbytes < 0) {
        perror("can raw socket read");
        // TODO: exception or error
//        return 0;
    }

    /* paranoid check ... */
    if (nbytes < sizeof(struct can_frame)) {
        fprintf(stderr, "read: incomplete CAN canFrame\n");
        // TODO: exception or error
//        return 0;
    }

//    int ret;
//
//    ret = recv(socket, &canFrame, sizeof(canFrame), 0);
//    if (ret != sizeof(canFrame)) {
//        if (ret < 0)
//            perror("recv failed");
//        else
//            fprintf(stderr, "recv returned %d", ret);
//        return -1;
//    }


//    for (int i = 0; i < 8; ++i) {
//        printf("%X", canFrame.data[i]);
//    }

//    printf("\n");
//    printf("id %d\n", canFrame.can_id);
//    printf("len8 %d\n", canFrame.len8_dlc);
////    printf("len %d\n", canFrame.len);
//    printf("dlc %d\n", canFrame.can_dlc);
//
//    printf("can end\n");

    frame.can_identifier = canFrame.can_id;
    frame.can_dlc = canFrame.can_dlc;
    frame.can_extbit = canFrame.can_id & CAN_EFF_FLAG;
    frame.can_rtr = canFrame.can_id & CAN_RTR_FLAG;

    memcpy((void *) frame.data, canFrame.data, sizeof(frame.data));
    /*end of user code*/
//    can_receive_message (&frame);


    // TODO: check memory returning (move or copy)
    return frame;
}

