#include <iostream>
#include "UniCAN.hpp"

using namespace UniCAN;

constexpr uint16_t UniCAN::CRC16::crc16tab[];

void Base::set_identifier(Message& message, Frame& frame, uint8_t data_bit)
{
    if ((message.getAddressFrom() > 31) || (message.getAddressTo() > 31))
    {
        frame.can_extbit = CAN_EXTENDED_HEADER;
        frame.can_identifier = 0x00;
        frame.can_identifier |= message.getAddressTo();
        frame.can_identifier |= message.getAddressFrom() << 14;
        if (data_bit != 0)
            frame.can_identifier |= 1 << 28;
    }
    else
    {
        frame.can_extbit = CAN_STANDART_HEADER;
        frame.can_identifier = 0x00;
        frame.can_identifier |= message.getAddressTo();
        frame.can_identifier |= message.getAddressFrom() << 5;
        if (data_bit != 0)
            frame.can_identifier |= 1 << 10;
    }
}

ErrorCode Base::check_frame(const Frame frame) {
    if (frame.can_dlc > UNICAN_MAX_DLC)
        return ErrorCode::UNICAN_CAN_MESSAGE_TOO_LONG;
    else if ((frame.can_identifier > 0x7FF) && (frame.can_extbit == 0))
        return ErrorCode::UNICAN_CAN_IDENTIFIER_TOO_LONG ;
    else if ((frame.can_identifier > 0x1FFFFFFF) && (frame.can_extbit > 0))
        return ErrorCode::UNICAN_CAN_IDENTIFIER_EXT_TOO_LONG ;
    else
        return ErrorCode::UNICAN_OK;
}

void Base::send(Message& message) {
    Frame frame{};

    if (online){
        frame.can_rtr = 0;
        if (message.getLength() < 7){
            set_identifier(message, frame, 0);
            frame.can_dlc = message.getLength() + UNICAN_MIN_DLC;
            frame.data[0]=(message.getId() & 0x00FF);
            frame.data[1]=((message.getId() >> 8) & 0x00FF);
            for (unsigned int i = 0; i < message.getLength(); i++)
                frame.data[i + 2] = message.getData()[i];
            HWsend(frame);
        } else {
            uint16_t crc;
            set_identifier(message, frame, 0);
            crc = CRC16::calculate(message.getData(), message.getLength() );
            frame.can_dlc = 6;
            frame.data[0] = UINT16RIGHT (UNICAN_START_LONG_MESSAGE );
            frame.data[1] = UINT16LEFT (UNICAN_START_LONG_MESSAGE );
            frame.data[2] = UINT16RIGHT (message.getId() );
            frame.data[3] = UINT16LEFT (message.getId() );
            frame.data[4] = UINT16RIGHT (message.getLength() + UNICAN_MIN_DLC);
            frame.data[5] = UINT16LEFT (message.getLength() + UNICAN_MIN_DLC);

            HWsend(frame);

            set_identifier(message, frame, 1);  //could be optimized

            uint16_t current_length;
            for (current_length = 0; current_length < message.getLength(); current_length++)
            {
                frame.data[current_length % 8] = message.getData()[current_length];
                if ((current_length % 8) ==7)
                {
                    frame.can_dlc = 8;
                    HWsend (frame);
                }
            }
            if (( current_length % 8) > 0) //something left
            {
                frame.can_dlc = current_length % 8;
                if ((current_length % 8) < 7) {
                    frame.data[current_length % 8] = UINT16RIGHT (crc );
                    frame.data[current_length % 8 + 1] = UINT16LEFT (crc );
                    frame.can_dlc+=2;
                    HWsend (frame);
                } else {
                    frame.data[7] = UINT16RIGHT (crc );
                    frame.can_dlc=8;
                    HWsend (frame);
                    frame.data[0] = UINT16LEFT (crc );
                    frame.can_dlc=1;
                    HWsend (frame);
                }
            }
            else{
                frame.data[0] = UINT16RIGHT (crc );
                frame.data[1] = UINT16LEFT (crc );
                frame.can_dlc=2;
                HWsend (frame);
            }
        }
    }
}

ErrorCode Base::collect_frame() {
    if (online)
    {
        Frame frame = HWreceive();

//        Packet *packet;

        //Reading CAN identifier fields
        uint16_t address_to;
        uint16_t address_from;
        uint8_t data_bit;
        uint16_t i;

        if (frame.can_extbit == 0)
        {
            address_to = (frame.can_identifier & 0x1F);
            address_from = (frame.can_identifier & 0x3E0) >> 5;
            data_bit = (frame.can_identifier & 0x400) >> 10;
        }
        else
        {
            address_to = (frame.can_identifier & 0x3FFF);
            address_from = (frame.can_identifier & 0xFFFC000) >> 14;
            data_bit = (frame.can_identifier & 0x10000000) >> 28;
        }
        //printf("Address_to = %d Address_from = %d, data_bit = %d \n ", address_to, address_from, data_bit );
        //Done with CAN identifier fields
        //Checking message
        ErrorCode errcode;
        errcode = check_frame(frame);
        if (errcode != ErrorCode::UNICAN_OK)
        {
//            unican_error(errcode);
            return errcode;
        }
        //Done with checking

        //Receive Logic:

        //Appending pure data to buffer
        if (data_bit == 1)
        {
//            packet = &active_transmit[address_from]; // return transmit, create if none
            //buff = unican_find_buffer(address_from);
//            if (buff == NULL)

            auto current_transmit_iterator = active_transmits.find(address_from);


            if (current_transmit_iterator == active_transmits.end())
            {
//                unican_error(ErrorCode::UNICAN_DATA_WITHOUT_START);
//                return ErrorCode::UNICAN_DATA_WITHOUT_START; // TODO: recover this error by creating message
                std::cerr << "UNICAN_DATA_WITHOUT_START NOT_FOUND_BUFFER" << std::endl;
            } else {
                Packet &current_transmit = current_transmit_iterator->second;

                if (current_transmit.message == nullptr)
                {
//                    unican_error(ErrorCode::UNICAN_DATA_WITHOUT_START);
                    std::cerr << "UNICAN_DATA_WITHOUT_START EMPTY_MESSAGE" << std::endl;

//                    return ErrorCode::UNICAN_DATA_WITHOUT_START;  // TODO: recover this error by creating message
                }
                else
                {
                    //printf("normal message with data\n");
                    for (i=0 ; i < frame.can_dlc ; i++)
                    {
                        if (current_transmit.current_position < current_transmit.message->getLength())
                        {
                            current_transmit.message->getData()[current_transmit.current_position] = frame.data[i];
                            current_transmit.current_position++;
                        }
                        else
                        {
//                            active_transmits.erase(current_transmit_iterator);
                            process_message(current_transmit_iterator);
                            // TODO: that buffer maybe should be erased
//                            unican_collect_buffer(buff);
//                            unican_error(ErrorCode::UNICAN_WARNING_UNEXPECTED_DATA);
                            return ErrorCode::UNICAN_WARNING_UNEXPECTED_DATA;//and what to do with buffer?
                        }
                    }
                    if (current_transmit.current_position == current_transmit.message->getLength())
                    {
                        return process_message(current_transmit_iterator);
                    }
                }
            }

            return ErrorCode::UNICAN_OK;
        }
            //Done with appending pure data to buffer
            //Processing some short commands
        else
        {
            //printf("message with command\n");
            uint16_t msg_id;
            if (frame.can_dlc >= UNICAN_MIN_DLC)
                msg_id = frame.data[0] + (frame.data[1] * 256);
            else
            {
//                unican_error(ErrorCode::UNICAN_CAN_MESSAGE_TOO_SHORT);
                return ErrorCode::UNICAN_CAN_MESSAGE_TOO_SHORT;
            }


            switch (msg_id)
            {
                //Some protocol-specific short messages
                case UNICAN_START_LONG_MESSAGE:
                {
                    //printf("Start of transmission\n");
                    if (frame.can_dlc < UNICAN_HEADER_SIZE)
                    {
//                        unican_error(ErrorCode::UNICAN_HEADER_TOO_SHORT);
                        return ErrorCode::UNICAN_HEADER_TOO_SHORT;
                    }

                    auto current_transmit_iterator = active_transmits.find(address_from);

//                    buff = unican_find_buffer(address_from);

                    if (current_transmit_iterator != active_transmits.end()){
                        // unican_drop_node(buff->node);
                        active_transmits.erase(current_transmit_iterator);
//                        unican_error(ErrorCode::UNICAN_WARNING_BUFFER_OVERWRITE);

                        return ErrorCode::UNICAN_WARNING_BUFFER_OVERWRITE;
                    }

//                    if (buff == nullptr)
//                    {
//                        unican_error(ErrorCode::UNICAN_NO_FREE_BUFFER);
//                        return;
//                    }

                    // TODO: memory allocation check, memory exception handling
                    Packet& current_transmit = active_transmits[address_from];

                    uint16_t msg_subid = frame.data[2] + (frame.data[3] * 256);
                    uint16_t data_length = frame.data[4] + (frame.data[5] * 256);

                    if (data_length < 2) {
//                        unican_error(ErrorCode::UNICAN_WRONG_CRC);
                        return ErrorCode::UNICAN_WRONG_CRC;
                    }

                    // TODO: memory allocation check, memory exception handling
                    current_transmit.message = new Message(msg_subid, address_from, address_to, data_length);
//                    buff->node = unican_allocate_node(msg_subid, address_from, address_to, data_length);
//                    if (buff->node == NULL)
//                    {
//                        unican_error(ErrorCode::UNICAN_CANT_ALLOCATE_NODE);
//                        return;
//                    }
//                    state.free_buffers_count--;
                    current_transmit.current_position = 0;

//                    packet->current_position = 0;
//                    buff->position = 0;


                } break;
                    //Done with protocol-specific short messages
                    //Regular short messages
                default:
                {
                    //printf("Standard command\n");
                    // TODO: memory allocation check, memory exception handling
                    auto message = new Message(msg_id, address_from, address_to, frame.can_dlc - UNICAN_MIN_DLC);


//                    unican_node* temp_node;
//                    temp_node = unican_allocate_node(msg_id, address_from, address_to, frame->can_dlc - CAN_MIN_DLC);
//                    if (temp_node == nullptr)
//                    {
//                        unican_error(ErrorCode::UNICAN_CANT_ALLOCATE_NODE);
//                        return;
//                    }

                    for ( i = 0; i<message->getLength(); i++)
                        message->getData()[i]=frame.data[i + UNICAN_MIN_DLC];

                    // TODO: check transfer of memory access
                    processed_messages.push(std::move(*message));
//                    unican_save_node (message);
                    delete message;

                } break;
                    //Done with regular short messages
            }
        }
        //Done with short commands
        return ErrorCode::UNICAN_OK;
    }
    else
        return ErrorCode::UNICAN_OFFLINE; // unican_error(ErrorCode::UNICAN_OFFLINE);

}

ErrorCode Base::process_message(std::map<uint16_t, UniCAN::Packet>::iterator active_transmit) {

//    active_transmits.extract(active_transmit);


    if (online) {
        Packet &current_packet = active_transmit->second;

        int len;
        uint16_t crc, calc_crc;

        len = current_packet.message->getLength(); // buff->node->value->unican_length;
        crc = current_packet.message->getData()[len - 2] + current_packet.message->getData()[len - 1] * 256;
        calc_crc = CRC16::calculate(current_packet.message->getData(), len - 2);

//        buff->crc= calc_crc;
        if (calc_crc == crc) {
            // TODO: check transfer of memory access
            processed_messages.push(std::move(*current_packet.message) );
            delete current_packet.message;

            current_packet.message = nullptr;

            active_transmits.erase(active_transmit);

//        unican_save_node(buff->node);
        } else {
            active_transmits.erase(active_transmit);
//            unican_drop_node(buff->node);
//            unican_error(ErrorCode::UNICAN_WRONG_CRC);

            return ErrorCode::UNICAN_WRONG_CRC;
        }

        return ErrorCode::UNICAN_OK;

//        buff->node = NULL;
//        buff->crc = 0x0000;
//        buff->position = 0;
//        state.free_buffers_count++;
    }
    else
        return ErrorCode::UNICAN_OFFLINE; // unican_error(ErrorCode::UNICAN_OFFLINE);

//    return Message();
}

Message Base::take_message() {
    Message last_message = processed_messages.back();
    processed_messages.pop();
    return last_message;
}


