
#ifndef _UNICAN_H
#define _UNICAN_H

#include <cstdint>
#include <string>
#include <map>
#include <queue>
#include <stdexcept>

#include "UniCAN_Utils.hpp"

namespace UniCAN{

    enum class ErrorCode{
        UNICAN_OK = 0,
        UNICAN_OFFLINE = 1,
        UNICAN_WARNING_BUFFER_OVERWRITE = 2,
        UNICAN_WRONG_CRC = 3,
        UNICAN_NO_FREE_BUFFER = 4,
        UNICAN_DATA_WITHOUT_START = 5,
        UNICAN_WARNING_UNEXPECTED_DATA = 6,

        UNICAN_CAN_MESSAGE_TOO_SHORT = 11,
        UNICAN_CAN_MESSAGE_TOO_LONG = 12,
        UNICAN_CAN_IDENTIFIER_TOO_LONG = 13,
        UNICAN_CAN_IDENTIFIER_EXT_TOO_LONG = 14,
        UNICAN_HEADER_TOO_SHORT = 15,
        UNICAN_CANT_ALLOCATE_NODE = 16,
        UNICAN_HW_ERROR = 17
    };

    class Message{
    private:
        uint16_t id; //MSG_ID of unican message
        uint16_t address_from; // address of sender in network
        uint16_t address_to; // address of receiver in network
        uint16_t length; //length of data
        uint8_t *data; //pointer to data field
    public:
        Message():id(0), address_from(0), address_to(0), length(0), data(nullptr){};
        Message(Message const &mes)
                : id(mes.id), address_from(mes.address_from), address_to(mes.address_to), length(mes.length), data(nullptr){
            if (length > 0) {
                data = new uint8_t[length];
                std::copy(mes.data, mes.data + mes.length, data);
            }
        }

        Message(Message &&mes) noexcept
                : id(mes.id), address_from(mes.address_from),
                  address_to(mes.address_to), length(mes.length), data(mes.data){
            mes.data = nullptr;
        }

        // TODO: constructor with all checks;
        Message(uint16_t id, uint16_t address_from, uint16_t address_to, uint16_t length)
                :id(id), address_from(address_from), address_to(address_to), data(nullptr), length(length){
            if (address_from > 31) throw std::out_of_range("Address_from must be between 0 and 31 (5bit)");
            if (address_to > 31) throw std::out_of_range("Address_to must be between 0 and 31 (5bit)");

            if (length > 0) {
                data = new uint8_t[length];
            }
        }

        ~Message(){ delete[] data; }


        uint16_t getId() const {
            return id;
        }

        void setId(uint16_t newId) {
            Message::id = newId;
        }

        uint16_t getAddressFrom() const {
            return address_from;
        }

        void setAddressFrom(uint16_t addressFrom) {
            // TODO: in CAN FD max is 16383 - 14bit
            if (addressFrom > 31) throw std::out_of_range("Address_from must be between 0 and 31 (5bit)");

            address_from = addressFrom;
        }

        uint16_t getAddressTo() const {
            return address_to;
        }

        void setAddressTo(uint16_t addressTo) {
            // TODO: in CAN FD max is 16383 - 14bit
            if (addressTo > 31) throw std::out_of_range("Address_to must be between 0 and 31 (5bit)");

            address_to = addressTo;
        }

        uint16_t getLength() const {
            return length;
        }

        uint8_t *getData() const {
            return data;
        }

        // takes pointer managing
        void setData(uint8_t *newData, uint16_t newLength) {
            if (newData != data){
                delete data;
                data = newData;
            }

            length = newLength;
        }
    };

    class Frame{
    public:  // TODO: constructor with all checks (DLC)
        uint32_t can_identifier;                     // 11 or 29bit CAN identifier
        uint8_t can_rtr;                             // Remote transmission request bit
        uint8_t can_extbit;                          // Identifier extension bit. 0x00 indicate 11 bit message ID
        uint8_t can_dlc;                             // Data length code. Number of bytes of data (0–8 bytes)
        uint8_t data[8];
    };

    class Packet{
    public:
        Message *message; //node with MSG accamulated in buffer
//        uint16_t crc; // CRC of message, 0 for short messages
        uint16_t current_position; //Current position in buffer
    };

    class Base{
//    private:
        // TODO: review scope of visibility

    protected:
        static const char UNICAN_MAX_DLC = 8; //maximum value of data length code
        const char UNICAN_MIN_DLC = 2; //minimum value of data length code
        const char UNICAN_HEADER_SIZE = 6;
        static const char CAN_STANDART_HEADER = 0;
        static const char CAN_EXTENDED_HEADER = 1;

        static const uint16_t UNICAN_START_LONG_MESSAGE = 0xFFFE;

        bool online;


        static void set_identifier(Message& message, Frame& frame, uint8_t data_bit);

        static uint16_t UINT16LEFT(const uint16_t val) { return (((val) >> 8) & 0x00FF); }
        static uint16_t UINT16RIGHT(const uint16_t val) { return ((val) & 0x00FF); }

        static ErrorCode check_frame(Frame frame);
    public:
        // TODO: change visibility
        std::map<uint16_t, Packet> active_transmits;
        std::queue<Message> processed_messages;
        // --

        virtual void open(std::string can_name) = 0;
        virtual void close() = 0;

        virtual void HWsend(Frame& frame) = 0;
        virtual Frame HWreceive() = 0;

        void send(Message& message);


        // TODO: error handling
        ErrorCode collect_frame();
        ErrorCode process_message(std::map<uint16_t, Packet>::iterator active_transmit);
        // --

        Message take_message();

        // Frame receive_bypass(); //receive frame without using frame buffer
    };

}

#endif //_UNICAN_H
