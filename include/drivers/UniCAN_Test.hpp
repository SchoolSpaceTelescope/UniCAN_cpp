
#ifndef _UNICAN_TEST_H
#define _UNICAN_TEST_H

#include "UniCAN.hpp"

namespace UniCAN{
    class TestCAN: public Base{
    public:
        Frame current_frame{};

        void open(std::string can_name) final { Base::online = true; };
        void close() final { Base::online = false; };

        void HWsend(Frame& frame) final {
            current_frame = frame;
        }

        Frame HWreceive() final {
            return current_frame;
        }
    };
}

#endif //_UNICAN_TEST_H
