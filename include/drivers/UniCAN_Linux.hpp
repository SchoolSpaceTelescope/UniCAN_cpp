
#ifndef _UNICAN_LINUX_H
#define _UNICAN_LINUX_H


#include "cstdint"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "UniCAN.hpp"

namespace UniCAN{
    class LinuxCAN: public Base{
    private:
        int s; /* can raw socket */
    public:
        void open(std::string can_name) final;
        void close() final { Base::online = false; ::close(s); };

        void HWsend(Frame& frame) final;

        Frame HWreceive() final;
    };
}

#endif //_UNICAN_LINUX_H
