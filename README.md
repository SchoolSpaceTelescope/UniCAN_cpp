# UniCAN++

Based on UniCAN protocol [implementation](https://github.com/Rinin/UniCAN) under [MIT License](https://github.com/Rinin/UniCAN/blob/trunk/LICENSE.txt), written on C.

Build project:

Old CMake (3.5):
```
mkdir -p bin/cmake-build-debug
cd bin/cmake-build-debug/
cmake ../..
make
```

New CMake (since 3.13):
```
cmake -B bin/cmake-build-debug/
cd bin/cmake-build-debug/
make
```
